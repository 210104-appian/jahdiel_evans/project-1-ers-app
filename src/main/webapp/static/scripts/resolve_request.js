// get all pending requests on page load
document.addEventListener("DOMContentLoaded", getAllPendingRequests);

// assign action to the back to pending list button click event
document.getElementById("back-to-pending-btn").addEventListener("click", backToPendingList);


function getAllPendingRequests() {
    performAjaxGetRequest(reimbursementsUrl + "?status=PENDING", handleSuccessfulRetrieval, handleUnsuccessfulRetrieval)
}

function handleSuccessfulRetrieval(responseText) {
    let pendingReimbursements = JSON.parse(responseText);
    let loggedInId = sessionStorage.getItem("token").split(":")[0];
    // reference to table body element
    let tableBody = document.getElementById("pending-requests-body");
    for(let pr of pendingReimbursements) { // add each pending request to the table body
        // make sure not to display pending requests owned by the logged in manager
        if(Number(loggedInId) != pr.employeeId) {
            // create reimbursement ID column
            let idDiv = document.createElement("div");
            idDiv.className = "text-end";
            idDiv.innerHTML = pr.reimbursementId
            let idColumn = document.createElement("th");
            idColumn.scope = "row";
            idColumn.appendChild(idDiv);
            // create employee name column
            let empNameColumn = document.createElement("td");
            empNameColumn.innerHTML = pr.employeeName;
            // create purchase date column
            let dateDiv = document.createElement("div");
            dateDiv.className = "text-end";
            let dateComponents = pr.purchaseDate.split("-");
            let dateVar = new Date(Number(dateComponents[0]), Number(dateComponents[1])-1, Number(dateComponents[2]));
            dateDiv.innerHTML = dateVar.toDateString();
            let dateColumn = document.createElement("td");
            dateColumn.appendChild(dateDiv);
            // create total cost column
            let costDiv = document.createElement("div");
            costDiv.className = "text-end";
            costDiv.innerHTML = "$" + pr.totalCost;
            let costColumn = document.createElement("td");
            costColumn.appendChild(costDiv);
            // create view details button column
            let viewDetailsBtn = document.createElement("button");
            viewDetailsBtn.className = "btn btn-success";
            viewDetailsBtn.value = JSON.stringify(pr);
            viewDetailsBtn.addEventListener("click", function() {
                displaySelectedRequest(viewDetailsBtn);
            });
            viewDetailsBtn.innerHTML = "View Details";
            let btnColumn = document.createElement("td");
            btnColumn.appendChild(viewDetailsBtn);
            // create table row
            let tableRow = document.createElement("tr");
            tableRow.appendChild(idColumn);
            tableRow.appendChild(empNameColumn);
            tableRow.appendChild(dateColumn);
            tableRow.appendChild(costColumn);
            tableRow.appendChild(btnColumn);
            // add new row to table body
            tableBody.appendChild(tableRow);
        }
    }
    // finally, display the main content
    document.getElementById("main-pending-content").hidden = false;
}

function handleUnsuccessfulRetrieval(statusCode) {
    // hide the table since no requests will be displayed
    document.getElementById("pending-requests-table").hidden = true;
    // show the "no pending requests" message if no requests were found
    if(statusCode > 399 && statusCode < 500) {
        document.getElementById("no-pending-requests").hidden = false;
    }
    // else show the "retrieval error" message if the server experienced an error
    else {
        document.getElementById("retrieval-error").hidden = false;
    }
    // finally, display the main content
    document.getElementById("main-pending-content").hidden = false;
}

function displaySelectedRequest(btn) {
    // hide the main content
    document.getElementById("main-pending-content").hidden = true;
    // hide any previous messages
    document.getElementById("success-card").hidden = true;
    document.getElementById("failure-card").hidden = true;
    // parse the JSON parameter (the button's value) to get the selected request
    sr = JSON.parse(btn.value);
    // assign the selected request's fields to the elements inside the request-details card
    document.getElementById("request-id").innerHTML = "Request ID# " + sr.reimbursementId;
    document.getElementById("employee-name").innerHTML = sr.employeeName;
    document.getElementById("description").innerHTML = sr.description;
    document.getElementById("total-cost").innerHTML = "$" + sr.totalCost;
    let dateComponents = sr.purchaseDate.split("-");
    let dateVar = new Date(Number(dateComponents[0]), Number(dateComponents[1])-1, Number(dateComponents[2]));
    document.getElementById("purchase-date").innerHTML = dateVar.toDateString();
    // assign methods to approve and deny buttons' click events
    let approveBtn = document.getElementById("approve-btn");
    approveBtn.onclick = function() {
        updateSelectedRequest(btn, "APPROVED");
    };
    let denyBtn = document.getElementById("deny-btn");
    denyBtn.onclick = function() {
        updateSelectedRequest(btn, "REJECTED");
    };
    // show the request-details card
    document.getElementById("request-details").hidden = false;
}

function updateSelectedRequest(btn, updateStatus) {
    // hide the request-details card
    document.getElementById("request-details").hidden = true;
    // parse the JSON parameter (the button's value) to get the selected request
    selReb = JSON.parse(btn.value);
    // store the updated reimbursement request's data into an object
    let reimbursementId = selReb.reimbursementId;
    let resolvedById = sessionStorage.getItem("token").split(":")[0];
    let status = updateStatus;
    let rbUpdate = {reimbursementId, resolvedById, status};
    performAjaxPutRequest(reimbursementsUrl, JSON.stringify(rbUpdate), handleSuccessfulUpdate, handleUnsuccessfulUpdate);
}

function handleSuccessfulUpdate() {
    // show success message
    document.getElementById("success-card").hidden = false;
    // delete the pending table records
    let pendingTable = document.getElementById("pending-requests-body");
    while(pendingTable.firstChild) {
        pendingTable.removeChild(pendingTable.firstChild);
    }
    // re-grab the pending requests from the DB
    getAllPendingRequests();
}

function handleUnsuccessfulUpdate() {
    // show fail message
    document.getElementById("failure-card").hidden = false;
    // show main content again
    document.getElementById("main-pending-content").hidden = false;
}

function backToPendingList() {
    // hide the request-details card
    document.getElementById("request-details").hidden = true;
    // show main content again
    document.getElementById("main-pending-content").hidden = false;

}