// get logged in employee's resolved requests on page load
document.addEventListener("DOMContentLoaded", getResolvedRequests);

function getResolvedRequests() {
    let employeeId = sessionStorage.getItem("token").split(":")[0];
    performAjaxGetRequest(reimbursementsUrl + "?status=RESOLVED&employeeId=" + employeeId, handleSuccessfulRetrieval, handleUnsuccessfulRetrieval)
}

function handleSuccessfulRetrieval(responseText) {
    let resolvedReimbursements = JSON.parse(responseText);
    // reference to table body element
    let tableBody = document.getElementById("resolved-requests-body");
    for(let rr of resolvedReimbursements) { // add each resolved request to the table body
        // create reimbursement ID column
        let idDiv = document.createElement("div");
        idDiv.className = "text-end";
        idDiv.innerHTML = rr.reimbursementId
        let idColumn = document.createElement("th");
        idColumn.scope = "row";
        idColumn.appendChild(idDiv);
        // create purchase date column
        let dateDiv = document.createElement("div");
        dateDiv.className = "text-end";
        let dateComponents = rr.purchaseDate.split("-");
        let dateVar = new Date(Number(dateComponents[0]), Number(dateComponents[1])-1, Number(dateComponents[2]));
        dateDiv.innerHTML = dateVar.toDateString();
        let dateColumn = document.createElement("td");
        dateColumn.appendChild(dateDiv);
        // create description column
        let descColumn = document.createElement("td");
        descColumn.innerHTML = rr.description;
        // create total cost column
        let costDiv = document.createElement("div");
        costDiv.className = "text-end";
        costDiv.innerHTML = "$" + rr.totalCost;
        let costColumn = document.createElement("td");
        costColumn.appendChild(costDiv);
        // create status column
        let statusColumn = document.createElement("td");
        statusColumn.innerHTML = rr.status;
        // create table row
        let tableRow = document.createElement("tr");
        tableRow.appendChild(idColumn);
        tableRow.appendChild(dateColumn);
        tableRow.appendChild(descColumn);
        tableRow.appendChild(costColumn);
        tableRow.appendChild(statusColumn);
        // add new row to table body
        tableBody.appendChild(tableRow);
    }
    // finally, display the main content
    document.getElementById("main-resolved-content").hidden = false;
}

function handleUnsuccessfulRetrieval(statusCode) {
    // hide the table since no requests will be displayed
    document.getElementById("resolved-requests-table").hidden = true;
    // show the "no resolved requests" message if no requests were found
    if(statusCode > 399 && statusCode < 500) {
        document.getElementById("no-resolved-requests").hidden = false;
    }
    // else show the "retrieval error" message if the server experienced an error
    else {
        document.getElementById("retrieval-error").hidden = false;
    }
    // finally, display the main content
    document.getElementById("main-resolved-content").hidden = false;
}