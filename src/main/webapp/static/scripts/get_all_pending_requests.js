// get all pending requests on page load
document.addEventListener("DOMContentLoaded", getAllPendingRequests);

function getAllPendingRequests() {
    performAjaxGetRequest(reimbursementsUrl + "?status=PENDING", handleSuccessfulRetrieval, handleUnsuccessfulRetrieval)
}

function handleSuccessfulRetrieval(responseText) {
    let pendingReimbursements = JSON.parse(responseText);
    // reference to table body element
    let tableBody = document.getElementById("pending-requests-body");
    for(let pr of pendingReimbursements) { // add each pending request to the table body
        // create reimbursement ID column
        let idDiv = document.createElement("div");
        idDiv.className = "text-end";
        idDiv.innerHTML = pr.reimbursementId
        let idColumn = document.createElement("th");
        idColumn.scope = "row";
        idColumn.appendChild(idDiv);
        // create employee name column
        let empNameColumn = document.createElement("td");
        empNameColumn.innerHTML = pr.employeeName;
        // create purchase date column
        let dateDiv = document.createElement("div");
        dateDiv.className = "text-end";
        let dateComponents = pr.purchaseDate.split("-");
        let dateVar = new Date(Number(dateComponents[0]), Number(dateComponents[1])-1, Number(dateComponents[2]));
        dateDiv.innerHTML = dateVar.toDateString();
        let dateColumn = document.createElement("td");
        dateColumn.appendChild(dateDiv);
        // create description column
        let descColumn = document.createElement("td");
        descColumn.innerHTML = pr.description;
        // create total cost column
        let costDiv = document.createElement("div");
        costDiv.className = "text-end";
        costDiv.innerHTML = "$" + pr.totalCost;
        let costColumn = document.createElement("td");
        costColumn.appendChild(costDiv);
        // create table row
        let tableRow = document.createElement("tr");
        tableRow.appendChild(idColumn);
        tableRow.appendChild(empNameColumn);
        tableRow.appendChild(dateColumn);
        tableRow.appendChild(descColumn);
        tableRow.appendChild(costColumn);
        // add new row to table body
        tableBody.appendChild(tableRow);
    }
    // finally, display the main content
    document.getElementById("main-pending-content").hidden = false;
}

function handleUnsuccessfulRetrieval(statusCode) {
    // hide the table since no requests will be displayed
    document.getElementById("pending-requests-table").hidden = true;
    // show the "no pending requests" message if no requests were found
    if(statusCode > 399 && statusCode < 500) {
        document.getElementById("no-pending-requests").hidden = false;
    }
    // else show the "retrieval error" message if the server experienced an error
    else {
        document.getElementById("retrieval-error").hidden = false;
    }
    // finally, display the main content
    document.getElementById("main-pending-content").hidden = false;
}