// redirect to login page if user loads this page without an auth token
document.addEventListener("DOMContentLoaded", function() {
    console.log("DOM Content Loaded");
    if(sessionStorage.getItem("token") == null) { // redirect to login page if user doesn't have auth token
        window.location.href = "login_page.html";
    }
    else {
        displayLoggedInName();
    }
});

function displayLoggedInName() {
    document.getElementById("navbarDropdown").innerText = sessionStorage.getItem("name");
}