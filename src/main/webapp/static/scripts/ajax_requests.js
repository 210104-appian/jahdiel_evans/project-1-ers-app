const baseUrl = "http://localhost:8080/ers-app";
const loginUrl = baseUrl + "/login";
const reimbursementsUrl = baseUrl + "/reimbursements";
const employeesUrl = baseUrl + "/employees";

// sends an AJAX GET request
function performAjaxGetRequest(url, successCallback, failureCallback) {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4) { // process HTTP response once it is done loading
            if(xhr.status > 199 && xhr.status < 300) { // if response returned success status
                successCallback(xhr.response);
            }
            else {
                if(failureCallback) {
                    failureCallback(xhr.status);
                }
                else {
                console.error("An error occurred while attempting to send a GET request. Status code: " + xhr.statusText);
                }
            }
        }
    }
    xhr.send();
}

// sends an AJAX POST request with a request body (payload)
function performAjaxPostRequest(url, payload, successCallback, failureCallback) {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4) { // process HTTP response once it is done loading
            if(xhr.status > 199 && xhr.status < 300) { // if response returned success status
                successCallback(xhr.response);
            }
            else {
                if(failureCallback) {
                    failureCallback(xhr.status);
                }
                else {
                    console.error("An error occurred while attempting to send a POST request. Status code: " + xhr.statusText);
                }
            }
        }
    }
    xhr.send(payload);
}

// sends an AJAX PUT request with a request body (payload)
function performAjaxPutRequest(url, payload, successCallback, failureCallback) {
    const xhr = new XMLHttpRequest();
    xhr.open("PUT", url);
    xhr.onreadystatechange = function() {
        if(xhr.readyState == 4) { // process HTTP response once it is done loading
            if(xhr.status > 199 && xhr.status < 300) { // if response returned success status
                successCallback(xhr.response);
            }
            else {
                if(failureCallback) {
                    failureCallback(xhr.status);
                }
                else {
                    console.error("An error occurred while attempting to send a POST request. Status code: " + xhr.statusText);
                }
            }
        }
    }
    xhr.send(payload);
}

