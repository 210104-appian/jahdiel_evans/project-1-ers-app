// assign the logout() method to the logout button's click event
document.getElementById("logout-btn").addEventListener("click", function (event) {
    console.log("cleaning the session storage.");
    sessionStorage.clear();
    window.location.href = "login_page.html";
    
});