document.addEventListener("DOMContentLoaded", function() {
    console.log("DOM Content Loaded");
    let token = sessionStorage.getItem("token");
    if(token != null) { // redirect to the proper homepage if user is already logged in
        let userRole = token.split(":")[1];
        // redirect to employee homepage if user has EMPLOYEE role
        if(userRole == "EMPLOYEE") {
            window.location.href = "employee_home.html";
        }
        // else redirect to manager homepage if user has MANAGER role
        else if(userRole == "MANAGER") {
            window.location.href = "manager_home.html";
        }
    }
});

// assign the attemptEmpLogin() function to the employee login button's click event
document.getElementById("emp-login-btn").addEventListener("click", attemptEmpLogin);

function attemptEmpLogin() {
    // hide any previous error messages
    document.getElementById("emp-error-msg").style.visibility = "hidden";
    document.getElementById("man-error-msg").style.visibility = "hidden";
    let empLoginForm = document.getElementById("emp-login-form");
    if(empLoginForm.reportValidity()) { // only submit the form if the input is valid
        let username = document.getElementById("emp-username-input").value;
        let password = document.getElementById("emp-password-input").value;
        let currentRole = document.getElementById("emp-login-btn").value;
        let credentials = {username, password, currentRole}
        performAjaxPostRequest(loginUrl, JSON.stringify(credentials), handleSuccessfulLogin, handleUnsuccessfulEmpLogin);
    }
    // untoggle the clicked Bootstrap button
    let button = document.getElementById('emp-login-btn');
    button.blur();
    
}

// assign the attemptManLogin() function to the manager login button's click event
document.getElementById("man-login-btn").addEventListener("click", attemptManLogin);

function attemptManLogin() {
    // hide any previous error messages
    document.getElementById("emp-error-msg").style.visibility = "hidden";
    document.getElementById("man-error-msg").style.visibility = "hidden";
    let manLoginForm = document.getElementById("man-login-form");
    if(manLoginForm.reportValidity()) { // only submit the form if the input is valid
        let username = document.getElementById("man-username-input").value;
        let password = document.getElementById("man-password-input").value;
        let currentRole = document.getElementById("man-login-btn").value;
        let credentials = {username, password, currentRole}
        performAjaxPostRequest(loginUrl, JSON.stringify(credentials), handleSuccessfulLogin, handleUnsuccessfulManLogin);
    }
    // untoggle the clicked Bootstrap button
    let button = document.getElementById('man-login-btn');
    button.blur();
}

// callback function for handling a successful login
function handleSuccessfulLogin(responseText) {
    let tokenAndName = responseText.split("-");
    sessionStorage.setItem("token", tokenAndName[0]); // create an auth token in browser session storage
    sessionStorage.setItem("name", tokenAndName[1]); // store the user's name in browser session storage
    let userRole = sessionStorage.getItem("token").split(":")[1] // grab the user's role
    if(userRole == "EMPLOYEE") {
        // let errorMsg = document.getElementById("emp-error-msg");
        // errorMsg.style.visibility = "hidden";
        window.location.href= "employee_home.html"; // redirect to employee dashboard page
    }
    else if(userRole == "MANAGER") {
        window.location.href= "manager_home.html"; // redirect to manager dashboard page
    }
}

// callback function for handling an unsuccessful employee login
function handleUnsuccessfulEmpLogin(responseStatus) {
    let errorMsg = document.getElementById("emp-error-msg");
    if(responseStatus == 401) { // display invalid credential message for 401 status
        errorMsg.innerText = "Invalid username or password.";
    }
    else if(responseStatus == 403) { // display invalid user privileges for 403 status
        errorMsg.innerText = "Insufficient user privileges for this login portal.";
    }
    errorMsg.style.visibility = "visible";
    // clear the credential input
    document.getElementById("emp-username-input").value = "";
    document.getElementById("emp-password-input").value = "";
}

// callback function for handling an unsuccessful manager login
function handleUnsuccessfulManLogin(responseStatus) {
    let errorMsg = document.getElementById("man-error-msg");
    if(responseStatus == 401) { // display invalid credential message for 401 status
        errorMsg.innerText = "Invalid username or password.";
    }
    else if(responseStatus == 403) { // display invalid user privileges for 403 status
        errorMsg.innerText = "Insufficient user privileges for this login portal.";
    }
    errorMsg.style.visibility = "visible";
    // clear the credential input
    document.getElementById("man-username-input").value = "";
    document.getElementById("man-password-input").value = "";
}