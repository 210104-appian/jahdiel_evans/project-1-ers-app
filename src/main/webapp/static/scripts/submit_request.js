// submit the expense reimbursement request when the submit button is pressed
document.getElementById("request-submit-btn").addEventListener("click", submitRequest)

function submitRequest() {
    // hide any previous pop-up messages
    document.getElementById("success-card").hidden = true;
    document.getElementById("failure-card").hidden = true;
    // attempt to submit the form
    let requestForm = document.getElementById("request-submit-form");
    if(requestForm.reportValidity()) { // only submit the request if the input is valid
        let purchaseDate = document.getElementById("purchase-date").value;
        let description = document.getElementById("description").value;
        let totalCost = document.getElementById("total-cost").value;
        let employeeId = sessionStorage.getItem("token").split(":")[0];
        let reimbursement = {employeeId, purchaseDate, description, totalCost}
        performAjaxPostRequest(reimbursementsUrl, JSON.stringify(reimbursement), handleSuccessfulSubmission, handleUnsuccessfulSubmission);
    }
    // untoggle the clicked Bootstrap button
    let button = document.getElementById('request-submit-btn');
    button.blur();
}

function handleSuccessfulSubmission() {
    // clear the form
    document.getElementById("purchase-date").value = "";
    document.getElementById("description").value = "";
    document.getElementById("total-cost").value = "";
    // show the success message
    document.getElementById("success-card").hidden = false;
}

function handleUnsuccessfulSubmission() {
    // clear the form
    document.getElementById("purchase-date").value = "";
    document.getElementById("description").value = "";
    document.getElementById("total-cost").value = "";
    // show the failure message
    document.getElementById("failure-card").hidden = false;
}