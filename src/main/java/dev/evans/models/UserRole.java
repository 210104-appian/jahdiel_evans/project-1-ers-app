package dev.evans.models;

public enum UserRole {
	
	EMPLOYEE, MANAGER;
	
}
