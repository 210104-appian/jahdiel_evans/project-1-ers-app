package dev.evans.models;

public class Reimbursement {

	private int reimbursementId;
	private int employeeId;
	private String employeeName;
	private int resolvedById;
	private String resolvedByName;
	private String purchaseDate; // will hold the date string yyyy-mm-dd
	private String description;
	private double totalCost;
	private Status status;
	
	public Reimbursement() {
		super();
	}

	public Reimbursement(int reimbursementId, int employeeId, String employeeName, int resolvedById,
			String resolvedByName, String purchaseDate, String description, double totalCost, Status status) {
		super();
		this.reimbursementId = reimbursementId;
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.resolvedById = resolvedById;
		this.resolvedByName = resolvedByName;
		this.purchaseDate = purchaseDate;
		this.description = description;
		this.totalCost = totalCost;
		this.status = status;
	}

	public int getReimbursementId() {
		return reimbursementId;
	}

	public void setReimbursementId(int reimbursementId) {
		this.reimbursementId = reimbursementId;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getResolvedById() {
		return resolvedById;
	}

	public void setResolvedById(int resolvedById) {
		this.resolvedById = resolvedById;
	}

	public String getResolvedByName() {
		return resolvedByName;
	}

	public void setResolvedByName(String resolvedByName) {
		this.resolvedByName = resolvedByName;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + employeeId;
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		result = prime * result + ((purchaseDate == null) ? 0 : purchaseDate.hashCode());
		result = prime * result + reimbursementId;
		result = prime * result + resolvedById;
		result = prime * result + ((resolvedByName == null) ? 0 : resolvedByName.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		long temp;
		temp = Double.doubleToLongBits(totalCost);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reimbursement other = (Reimbursement) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (employeeId != other.employeeId)
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		if (purchaseDate == null) {
			if (other.purchaseDate != null)
				return false;
		} else if (!purchaseDate.equals(other.purchaseDate))
			return false;
		if (reimbursementId != other.reimbursementId)
			return false;
		if (resolvedById != other.resolvedById)
			return false;
		if (resolvedByName == null) {
			if (other.resolvedByName != null)
				return false;
		} else if (!resolvedByName.equals(other.resolvedByName))
			return false;
		if (status != other.status)
			return false;
		if (Double.doubleToLongBits(totalCost) != Double.doubleToLongBits(other.totalCost))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Reimbursement [reimbursementId=" + reimbursementId + ", employeeId=" + employeeId + ", employeeName="
				+ employeeName + ", resolvedById=" + resolvedById + ", resolvedByName=" + resolvedByName
				+ ", purchaseDate=" + purchaseDate + ", description=" + description + ", totalCost=" + totalCost
				+ ", status=" + status + "]";
	}
	
	

}
