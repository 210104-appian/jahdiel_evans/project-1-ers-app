package dev.evans.models;

public enum Status {

	SUBMITTED, APPROVED, REJECTED
	
}
