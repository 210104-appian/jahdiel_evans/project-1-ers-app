package dev.evans.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dev.evans.models.User;
import dev.evans.models.UserRole;
import dev.evans.util.ConnectionUtil;
import dev.evans.util.LoggerUtil;

public class UserDAOImpl implements UserDAO {
	
	// singleton reference to UserDAO
	private static final UserDAO uDAO = new UserDAOImpl();
	
	private UserDAOImpl() {
		super();
	}
	
	// returns singleton reference
	public static UserDAO getUserDAO() {
		return uDAO;
	}

	@Override
	public User getUserByCredentials(String username, String password) {
		User user = null;
		String sql = "SELECT ID, FIRST_NAME, LAST_NAME " +
					 "FROM PROJECT_1.COMPANY_USER " +
					 "WHERE UPPER(USERNAME) = UPPER(?) AND PASSWORD = ?";

		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setString(1, username);
			pStatement.setString(2, password);
			ResultSet rs = pStatement.executeQuery(); // look for user with matching credentials
			if(rs.next()) { // if a user is returned from the DB, store and return their ID
				user = new User();
				user.setUserId(rs.getInt("ID"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				LoggerUtil.getLogger().info("Found user with ID = "+user.getUserId() + " in the DB");
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
			LoggerUtil.getLogger().error("Failed to retrieve a user by credentials from the DB. Exception thrown: " + e.getClass());
		}

		return user;
	}
	
	@Override
	public User getUserById(int userId) {
		User user = null;
		String sql = "SELECT * " +
					 "FROM PROJECT_1.COMPANY_USER " +
					 "WHERE ID = ?";

		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setInt(1, userId);
			ResultSet rs = pStatement.executeQuery(); // look for user with matching ID
			if(rs.next()) { // if a user is returned from the DB, store and return their data (minus the credentials)
				user = new User();
				user.setUserId(rs.getInt("ID"));
				user.setEmail(rs.getString("EMAIL"));
				user.setFirstName(rs.getString("FIRST_NAME"));
				user.setLastName(rs.getString("LAST_NAME"));
				user.setJobTitle(rs.getString("JOB_TITLE"));
				user.setMobileNo(rs.getString("MOBILE_NO"));
				LoggerUtil.getLogger().info("Found user with ID = "+user.getUserId() + " in the DB");
			}
		}
		catch(SQLException e) {
			e.printStackTrace();
			LoggerUtil.getLogger().error("Failed to retrieve a user by ID from the DB. Exception thrown: " + e.getClass());
		}

		return user;
	}


	@Override
	public List<UserRole> getUserRoles(int userId) {
		List<UserRole> roles = new ArrayList<>();
		String sql = "SELECT cr.ROLE_NAME " +
					 "FROM PROJECT_1.COMPANY_USER_ROLE cur " +
					 "INNER JOIN PROJECT_1.COMPANY_ROLE cr " +
					 "ON cur.ROLE_ID = cr.ID " +
					 "WHERE cur.USER_ID = ?";
		
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setInt(1, userId);
			// get all roles associated with the user's id
			ResultSet rs = pStatement.executeQuery();
			while(rs.next()) {
				String userRoleString = rs.getString(1); // get the next record's user role string
				boolean isValidRole = Arrays.stream(
						UserRole.values())
						.map((value)->value.toString())
						.anyMatch((str)->str.equals(userRoleString)); // check to see if the string value is found in the enum before parsing
				if(isValidRole) {
					roles.add(UserRole.valueOf(userRoleString)); // convert the string to an enum and add it to the list
				}
			}
			LoggerUtil.getLogger().info(String.format("Found the following roles in the DB for user ID = %d: %s", userId, roles.toString()));
		}
		catch(SQLException e) {
			e.printStackTrace();
			LoggerUtil.getLogger().error("Failed to retrieve user roles by ID from the DB. Exception thrown: " + e.getClass());
		}
		
		return roles;
	}
	
	public UserRole getHighestRole(int userId) {
		UserRole highestRole = null;
		String sql = "SELECT ROLE_NAME " +
					 "FROM PROJECT_1.COMPANY_ROLE " +
					 "WHERE ID = " +
					 "(SELECT MAX(cur.ROLE_ID) " +
					 "FROM PROJECT_1.COMPANY_USER_ROLE cur " +
					 "INNER JOIN PROJECT_1.COMPANY_ROLE cr " +
					 "ON cur.ROLE_ID = cr.ID " +
					 "WHERE cur.USER_ID = ?)";
		
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setInt(1, userId);
			// get the highest role associated with the user's id
			ResultSet rs = pStatement.executeQuery();
			if(rs.next()) {
				String userRoleString = rs.getString(1); // get the highest user role string
				boolean isValidRole = Arrays.stream(
						UserRole.values())
						.map((value)->value.toString())
						.anyMatch((str)->str.equals(userRoleString)); // check to see if the string value is found in the enum before parsing
				if(isValidRole) {
					highestRole = UserRole.valueOf(userRoleString); // convert the string to an enum and assign it to highest role
				}
			}
			LoggerUtil.getLogger().info(String.format("User with ID = %d has the following highest role: %s", userId, highestRole.toString()));
		}
		catch(SQLException e) {
			e.printStackTrace();
			LoggerUtil.getLogger().error("Failed to retrieve the highest user role by ID from the DB. Exception thrown: " + e.getClass());
		}
		
		return highestRole;
	}

}
