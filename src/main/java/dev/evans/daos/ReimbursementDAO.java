package dev.evans.daos;

import java.util.List;

import dev.evans.models.Reimbursement;
import dev.evans.models.Status;

public interface ReimbursementDAO {
	
	public boolean addReimbursement(Reimbursement rb);
	public List<Reimbursement> getPendingReimbursementsByUserId(int userId);
	public List<Reimbursement> getResolvedReimbursementsByUserId(int userId);
	public List<Reimbursement> getAllPendingReimbursements();
	public List<Reimbursement> getAllResolvedReimbursements();
	public boolean updateReimbursementById(int reimbId, int managerId, Status status);

}
