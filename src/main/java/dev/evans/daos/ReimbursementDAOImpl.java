package dev.evans.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dev.evans.models.Reimbursement;
import dev.evans.models.Status;
import dev.evans.util.ConnectionUtil;
import dev.evans.util.IOUtil;
import dev.evans.util.LoggerUtil;

public class ReimbursementDAOImpl implements ReimbursementDAO {
	
	// singleton reference to ReimbursementDAO
	private static final ReimbursementDAO rDAO = new ReimbursementDAOImpl();

	private ReimbursementDAOImpl() {
		super();
	}
	
	// returns singleton reference
	public static ReimbursementDAO getReimbursementDAO() {
		return rDAO;
	}

	@Override
	public boolean addReimbursement(Reimbursement rb) {
		boolean success = false;
		// first check that the reimbursement object isn't null
		if(rb != null) {
			// then make sure that the values to be inserted are valid
			if(rb.getEmployeeId() > 0 && rb.getPurchaseDate() != null && rb.getDescription() != null && rb.getTotalCost() > 0  && rb.getTotalCost() <= IOUtil.MAX_COST) {
				// finally check that date string and total cost are formatted correctly
				if(rb.getPurchaseDate().matches("^\\d{4}-\\d{2}-\\d{2}$") && Double.toString(rb.getTotalCost()).matches("^\\d+\\.\\d{0,2}$")) {
					int rowCount = 0;
					String sql = "INSERT INTO PROJECT_1.REIMBURSEMENT " +
							"(EMPLOYEE_ID, PURCHASE_DATE, DESCRIPTION, TOTAL_COST) " +
							"VALUES (?, TO_DATE(?, 'YYYY-MM-DD'), ?, ?)";
					try(Connection connection = ConnectionUtil.getConnection();
							PreparedStatement pStatement = connection.prepareStatement(sql);) {
						pStatement.setInt(1, rb.getEmployeeId());
						pStatement.setString(2, rb.getPurchaseDate());
						pStatement.setString(3, rb.getDescription());
						pStatement.setDouble(4, rb.getTotalCost());
						// insert the new reimbursement into the DB
						rowCount = pStatement.executeUpdate(); // a successful insert returns a row count of 1
					}
					catch(SQLException e) {
						e.printStackTrace();
						LoggerUtil.getLogger().error("Failed to insert a new reimbursement into the DB. Exception thrown: " + e.getClass());
					}
					// if row count == 1, then the insert was successful
					if(rowCount == 1) {
						success = true;
						LoggerUtil.getLogger().info(String.format("Employee ID# %d has submitted a new reimbursement request"
								+ " - Purchase Date: %s, Description: %s, Total Cost: $%,.2f", rb.getEmployeeId(), rb.getPurchaseDate(), rb.getDescription(), rb.getTotalCost()));
					}
				}
			}
		}
		return success;
	}

	@Override
	public List<Reimbursement> getPendingReimbursementsByUserId(int userId) {
		List<Reimbursement> rbs = null;
		// first check that the userId is positive
		if(userId > 0) {
			String sql = "SELECT r.ID, r.EMPLOYEE_ID, cu.FIRST_NAME || ' ' || cu.LAST_NAME AS EMPLOYEE_NAME, r.PURCHASE_DATE, r.DESCRIPTION, r.TOTAL_COST " +
						 "FROM PROJECT_1.REIMBURSEMENT r " +
						 "INNER JOIN PROJECT_1.COMPANY_USER cu " +
						 "ON r.EMPLOYEE_ID = cu.ID " +
						 "WHERE EMPLOYEE_ID = ? AND STATUS = 'SUBMITTED'";
			try(Connection connection = ConnectionUtil.getConnection();
					PreparedStatement pStatement = connection.prepareStatement(sql)) {
				pStatement.setInt(1, userId);
				ResultSet rs = pStatement.executeQuery();
				rbs = processPendingReimbursementResultSet(rs);
			}
			catch(SQLException e) {
				e.printStackTrace();
				LoggerUtil.getLogger().error("Failed to retrieve pending reimbursements from the DB. Exception thrown: " + e.getClass());
			}
		}
		return rbs;
	}
	
	private List<Reimbursement> processPendingReimbursementResultSet(ResultSet rs) throws SQLException {
		List<Reimbursement> rbs = new ArrayList<>();
		while(rs.next()) {
			Reimbursement rb = new Reimbursement();
			rb.setReimbursementId(rs.getInt("ID"));
			rb.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
			rb.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
			rb.setPurchaseDate(rs.getDate("PURCHASE_DATE").toString());
			rb.setDescription(rs.getString("DESCRIPTION"));
			rb.setTotalCost(rs.getDouble("TOTAL_COST"));
			rbs.add(rb);
		}
		return rbs;
	}

	@Override
	public List<Reimbursement> getResolvedReimbursementsByUserId(int userId) {
		List<Reimbursement> rbs = null;
		// first check that the userId is positive
		if(userId > 0) {
			String sql = "SELECT r.ID, r.EMPLOYEE_ID, cu.FIRST_NAME || ' ' || cu.LAST_NAME AS EMPLOYEE_NAME, r.PURCHASE_DATE, r.DESCRIPTION, r.TOTAL_COST, " +
						 "cu2.FIRST_NAME || ' ' || cu2.LAST_NAME AS MANAGER_NAME, r.STATUS " +
						 "FROM PROJECT_1.REIMBURSEMENT r " +
						 "INNER JOIN PROJECT_1.COMPANY_USER cu " +
						 "ON r.EMPLOYEE_ID = cu.ID " +
						 "INNER JOIN PROJECT_1.COMPANY_USER cu2 " +
						 "ON r.RESOLVED_BY_ID = cu2.ID " +
						 "WHERE EMPLOYEE_ID = ? AND NOT STATUS = 'SUBMITTED'";
			try(Connection connection = ConnectionUtil.getConnection();
					PreparedStatement pStatement = connection.prepareStatement(sql)) {
				pStatement.setInt(1, userId);
				ResultSet rs = pStatement.executeQuery();
				rbs = processResolvedReimbursementResultSet(rs);
			}
			catch(SQLException e) {
				e.printStackTrace();
				LoggerUtil.getLogger().error("Failed to retrieve resolved reimbursements from the DB. Exception thrown: " + e.getClass());
			}
		}
		return rbs;
	}
	
	private List<Reimbursement> processResolvedReimbursementResultSet(ResultSet rs) throws SQLException {
		List<Reimbursement> rbs = new ArrayList<>();
		while(rs.next()) {
			Reimbursement rb = new Reimbursement();
			rb.setReimbursementId(rs.getInt("ID"));
			rb.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
			rb.setEmployeeName(rs.getString("EMPLOYEE_NAME"));
			rb.setPurchaseDate(rs.getDate("PURCHASE_DATE").toString());
			rb.setDescription(rs.getString("DESCRIPTION"));
			rb.setTotalCost(rs.getDouble("TOTAL_COST"));
			rb.setResolvedByName(rs.getString("MANAGER_NAME"));
			String statusString = rs.getString("STATUS");
			boolean isValidStatus = Arrays.stream(
					Status.values())
					.map((value)->value.toString())
					.anyMatch((str)->str.equals(statusString)); // checking to see if the value is found in the enum before parsing
			if(isValidStatus) {
				rb.setStatus(Status.valueOf(statusString));
			}
			rbs.add(rb);
		}
		return rbs;
	}

	@Override
	public List<Reimbursement> getAllPendingReimbursements() {
		List<Reimbursement> rbs = null;
		String sql = "SELECT r.ID, r.EMPLOYEE_ID, cu.FIRST_NAME || ' ' || cu.LAST_NAME AS EMPLOYEE_NAME, r.PURCHASE_DATE, r.DESCRIPTION, r.TOTAL_COST " +
				"FROM PROJECT_1.REIMBURSEMENT r " +
				"INNER JOIN PROJECT_1.COMPANY_USER cu " +
				"ON r.EMPLOYEE_ID = cu.ID " +
				"WHERE STATUS = 'SUBMITTED'";
		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery(sql);
			rbs = processPendingReimbursementResultSet(rs);
		}
		catch(SQLException e) {
			e.printStackTrace();
			LoggerUtil.getLogger().error("Failed to retrieve all pending reimbursements from the DB. Exception thrown: " + e.getClass());
		}
		return rbs;
	}

	@Override
	public List<Reimbursement> getAllResolvedReimbursements() {
		List<Reimbursement> rbs = null;
		String sql = "SELECT r.ID, r.EMPLOYEE_ID, cu.FIRST_NAME || ' ' || cu.LAST_NAME AS EMPLOYEE_NAME, r.PURCHASE_DATE, r.DESCRIPTION, r.TOTAL_COST, " +
				 "cu2.FIRST_NAME || ' ' || cu2.LAST_NAME AS MANAGER_NAME, r.STATUS " +
				 "FROM PROJECT_1.REIMBURSEMENT r " +
				 "INNER JOIN PROJECT_1.COMPANY_USER cu " +
				 "ON r.EMPLOYEE_ID = cu.ID " +
				 "INNER JOIN PROJECT_1.COMPANY_USER cu2 " +
				 "ON r.RESOLVED_BY_ID = cu2.ID " +
				 "WHERE NOT STATUS = 'SUBMITTED'";
		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery(sql);
			rbs = processResolvedReimbursementResultSet(rs);
		}
		catch(SQLException e) {
			e.printStackTrace();
			LoggerUtil.getLogger().error("Failed to retrieve all resolved reimbursements from the DB. Exception thrown: " + e.getClass());
		}
		return rbs;
	}

	@Override
	public boolean updateReimbursementById(int reimbId, int managerId, Status status) {
		boolean success = false;
		// first check that the parameters are valid
		if(reimbId > 0 && managerId > 0 && status != null) {
			int rowCount = 0;
			String sql = "UPDATE PROJECT_1.REIMBURSEMENT " +
						 "SET RESOLVED_BY_ID = ?, STATUS = ? " +
						 "WHERE ID = ?";
			try(Connection connection = ConnectionUtil.getConnection();
					PreparedStatement pStatement = connection.prepareStatement(sql)) {
				pStatement.setInt(3, reimbId);
				pStatement.setInt(1, managerId);
				pStatement.setString(2, status.toString());
				rowCount = pStatement.executeUpdate();
			}
			catch(SQLException e) {
				e.printStackTrace();
				LoggerUtil.getLogger().error("Failed to update a reimbursement in the DB. Exception thrown: " + e.getClass());
			}
			// if row count == 1, then the update was successful
			if(rowCount == 1) {
				success = true;
				LoggerUtil.getLogger().info(String.format("Manager ID# %d has resolved a reimbursement request"
						+ " - Reimbursement ID#: %d, Status: %s", managerId, reimbId, status.toString()));
			}
		}
		return success;
	}

}
