package dev.evans.daos;

import java.util.List;

import dev.evans.models.User;
import dev.evans.models.UserRole;

public interface UserDAO {
	
	public User getUserByCredentials(String username, String password);
	public User getUserById(int userId);
	public List<UserRole> getUserRoles(int userId);
	public UserRole getHighestRole(int userId);
	
}
