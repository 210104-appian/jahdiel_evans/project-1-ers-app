package dev.evans.util;

import com.fasterxml.jackson.databind.ObjectMapper;

public class IOUtil {
	
	public static final double MAX_COST = 999999.99;

	// static reference to the object mapper
	private static ObjectMapper oMapper;

	public static ObjectMapper getObjMapper() {
		if(oMapper == null) {
			oMapper = new ObjectMapper();
		}
		return oMapper;
	}

}
