package dev.evans.util;

import org.apache.log4j.Logger;

public class LoggerUtil {
	
	// static reference to the logger
	private static Logger logger;

	public static Logger getLogger() {
		if(logger == null) {
			logger = Logger.getRootLogger();
		}
		return logger;
	}

}
