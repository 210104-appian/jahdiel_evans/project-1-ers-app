package dev.evans.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

	private static Connection connection;

	public static Connection getHardCodedConnection() throws SQLException {
		if(connection == null || connection.isClosed()) {
			String url = "jdbc:oracle:thin:@//bank-app-database.capsg0iyx6vb.us-east-2.rds.amazonaws.com:1521/ORCL";
			String username = "project_1";
			String password = "3xpense&3092";
			connection = DriverManager.getConnection(url, username, password);
		}
		return connection;
	}

	/*
	 * Can also use a properties file (https://www.java2blog.com/wp-content/uploads/2016/02/configFileScreenshot.png)
	 * which stores key value pairs. Then we can add this file to our .gitignore so that it's not uploaded.
	 */
	public static Connection getConnection() throws SQLException {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver"); //registering Oracle Driver
		}
		catch(ClassNotFoundException ex) {
			ex.printStackTrace();
			//IOUtil.getLogger().error("Unable to locate Oracle JDBC driver. Thrown exception: " + ex.getClass());
			System.exit(1);
		}

		if(connection == null || connection.isClosed()) {
			String url = System.getenv("DB_URL"); // best not to hard code in your db credentials ! 
			String username = System.getenv("DB_USERNAME");
			String password = System.getenv("DB_PASSWORD");			
			connection = DriverManager.getConnection(url, username, password);
		}
		return connection;
	}

}
