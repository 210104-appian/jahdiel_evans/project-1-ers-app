package dev.evans.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dev.evans.services.UserService;
import dev.evans.services.UserServiceImpl;
import dev.evans.util.IOUtil;
import dev.evans.models.User;

public class EmployeeServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// reference to UserService singleton
	private UserService uService;
	
	public EmployeeServlet() {
		super();
		uService = UserServiceImpl.getUserService();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String uri = request.getRequestURI();
		String[] splitUri = uri.split("/");
		if(splitUri.length == 4) { // if there is a "user ID" in the URI, get a single employee
			String idStr = splitUri[3];
			System.out.println("User ID requested: "+idStr);
			
			// if the "user ID" sent is an actual integer, attempt to find the user
			if(idStr != null && idStr.matches("^\\d+$")) {
				int id = Integer.parseInt(idStr);	
				User user = uService.findUserById(id); // find the user by ID
				// if a user was found, send it back in the response body
				if(user != null) {
					try(PrintWriter pw = response.getWriter()) {
						pw.write(IOUtil.getObjMapper().writeValueAsString(user));
					}
				}
				// else send error code saying resource not found
				else {
					response.sendError(404, "user could not be found by these credentials");
				}
			}
			// else send error code saying bad request
			else {
				response.sendError(400, "userId is invalid");
			}
		}
		// else get all employees (currently unavailable)
		else { 
			response.sendError(503, "Request not yet implemented");
		}
	}

}
