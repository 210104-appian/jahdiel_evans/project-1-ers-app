package dev.evans.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dev.evans.models.Reimbursement;
import dev.evans.services.ReimbursementService;
import dev.evans.services.ReimbursementServiceImpl;
import dev.evans.util.IOUtil;

public class ReimbursementServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// reference to ReimbursementService singleton
	private ReimbursementService rService;
	
	public ReimbursementServlet() {
		super();
		rService = ReimbursementServiceImpl.getReimbursementService();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String status = request.getParameter("status");
		String employeeId = request.getParameter("employeeId");
		System.out.println(status + " " + employeeId);
		
		List<Reimbursement> rbs = null;
		// get reimbursements by employee ID and status if both parameters are valid
		if(status != null && employeeId != null && employeeId.matches("^\\d+$")) {
			int id = Integer.parseInt(employeeId);
			System.out.println(id + " is an integer");
			if(status.equals("PENDING") || status.equals("RESOLVED")) {
				if(status.equals("PENDING")) { // get pending reimbursements by emp id
					System.out.println("status equals \"PENDING\"");
					rbs = rService.findPendingReimbursementsByUserId(id);
				}
				else if(status.equals("RESOLVED")) { // or get resolved reimbursements by emp id
					rbs = rService.findResolvedReimbursementsByUserId(id);
					System.out.println("status equals \"RESOLVED\"");
				}
				
				if(rbs != null && rbs.size() > 0) { // return the list of reimbursements if the search was successful
					try(PrintWriter pw = response.getWriter()) {
						String rbsJSON = IOUtil.getObjMapper().writeValueAsString(rbs);
						pw.write(rbsJSON);
					}
				}
				else if(rbs != null){ // else no reimbursements were found if list size is 0
					response.sendError(404, "No reimbursements were found");
					System.out.println("No reimbursements were found");
				}
				else { // else the server had an issue retrieving the reimbursements
					response.sendError(500, "Server encountered an error while accessing the database");
					System.out.println("Server encountered an error while accessing the database");
				}
			}
			else { // send back 400 status (bad request)
				response.sendError(400, "Status wasn't PENDING or RESOLVED");
				System.out.println("Status wasn't PENDING or RESOLVED");
			}
		}
		// else get reimbursements by status if just the status parameter is valid
		else if(status != null) {
			if(status.equals("PENDING") || status.equals("RESOLVED")) {
				if(status.equals("PENDING")) { // get all pending reimbursements
					System.out.println("status equals \"PENDING\"");
					rbs = rService.findAllPendingReimbursements();
				}
				else if(status.equals("RESOLVED")) { // or get all resolved reimbursements
					rbs = rService.findAllResolvedReimbursements();
					System.out.println("status equals \"RESOLVED\"");
				}
				
				if(rbs != null && rbs.size() > 0) { // return the list of reimbursements if the search was successful
					try(PrintWriter pw = response.getWriter()) {
						String rbsJSON = IOUtil.getObjMapper().writeValueAsString(rbs);
						pw.write(rbsJSON);
					}
				}
				else if(rbs != null){ // else no reimbursements were found if list size is 0
					response.sendError(404, "No reimbursements were found");
					System.out.println("No reimbursements were found");
				}
				else { // else the server had an issue retrieving the reimbursements
					response.sendError(500, "Server encountered an error while accessing the database");
					System.out.println("Server encountered an error while accessing the database");
				}
			}
			else { // send back 400 status (bad request)
				response.sendError(400, "Status wasn't PENDING or RESOLVED");
				System.out.println("Status wasn't PENDING or RESOLVED");
			}
		}
		// else send back 400 status (bad request)
		else {
			response.sendError(400, "Parameters are null/invalid");
			System.out.println("Parameters are null/invalid");
		}
		
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String rbJSON = request.getReader().readLine();
		Reimbursement rb = IOUtil.getObjMapper().readValue(rbJSON, Reimbursement.class);
		
		// create a new reimbursement request in the DB
		boolean success = rService.submitNewReimbursement(rb);
		
		if(success) { // send back a 201 if the reimbursement was successfully inserted
			response.setStatus(201);
		}
		else { // else respond with 400 status for a bad request (wrong syntax)
			response.sendError(400);
		}
	}
	
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String rbJSON = request.getReader().readLine();
		System.out.println(rbJSON);
		Reimbursement rbUpdate = IOUtil.getObjMapper().readValue(rbJSON, Reimbursement.class);
		
		// update a reimbursement request in the DB
		boolean success = rService.resolveReimbursement(rbUpdate.getReimbursementId(), rbUpdate.getResolvedById(), rbUpdate.getStatus());
		
		if(success) { // send back a 200 if the reimbursement was successfully updated
			response.setStatus(200);
		}
		else { // else respond with 400 status for a bad request
			response.sendError(400, "Update unsuccessful, reimbursement info was incorrect");
		}
	}

}
