package dev.evans.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dev.evans.services.UserService;
import dev.evans.services.UserServiceImpl;
import dev.evans.util.IOUtil;
import dev.evans.util.LoggerUtil;
import dev.evans.models.User;

public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// reference to UserService singleton
	private UserService uService;
	
	public LoginServlet() {
		super();
		uService = UserServiceImpl.getUserService();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		//RequestDispatcher rd = request.getRequestDispatcher("static/html/login_page.html");
		//rd.forward(request, response);
		response.sendRedirect("static/html/login_page.html");
	}
	 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		User credentials = IOUtil.getObjMapper().readValue(request.getReader().readLine(), User.class);
		
		// verify if the credentials match a user in the database
		User user = uService.verifyUser(credentials.getUsername(), credentials.getPassword(), credentials.getCurrentRole());
		
		
		
		if(user != null) { // If a user matched the sent credentials, check if the matching user has the right role to log in
			// If the matching user has the right role, send back an access token
			if(user.getCurrentRole() != null) {
				String token = user.getUserId()+":"+user.getCurrentRole();
				String fullName = user.getFirstName() + " " + user.getLastName();
				String responseBody = token + "-" + fullName;
				try(PrintWriter pw = response.getWriter()){
					pw.write(responseBody);
				}
				// debug logging that tracks successful logins
				//System.out.println(String.format("User ID = %d : logged in with current role = %s", user.getUserId(), user.getCurrentRole().toString()));
				LoggerUtil.getLogger().info(String.format("User ID = %d - logged in with current role = %s", user.getUserId(), user.getCurrentRole().toString()));
			}
			else { // else respond with 403 status for incorrect permissions
				response.sendError(403, "Insufficient user privileges");
			}
		}
		else { // else respond with 401 status for incorrect credentials
			response.sendError(401, "Username and/or password is incorrect");
		}
	
	}

}
