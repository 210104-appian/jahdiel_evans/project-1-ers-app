package dev.evans.services;

import java.util.List;

import dev.evans.models.Reimbursement;
import dev.evans.models.Status;

public interface ReimbursementService {
	
	public boolean submitNewReimbursement(Reimbursement rb);
	public List<Reimbursement> findPendingReimbursementsByUserId(int userId);
	public List<Reimbursement> findResolvedReimbursementsByUserId(int userId);
	public List<Reimbursement> findAllPendingReimbursements();
	public List<Reimbursement> findAllResolvedReimbursements();
	public boolean resolveReimbursement(int reimbId, int managerId, Status status);

}
