package dev.evans.services;

import dev.evans.models.User;
import dev.evans.models.UserRole;

public interface UserService {
	
	public User verifyUser(String username, String password, UserRole loginRole);
	public User findUserById(int userId);
	
}
