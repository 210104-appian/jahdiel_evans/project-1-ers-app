package dev.evans.services;

import java.util.List;

import dev.evans.daos.ReimbursementDAO;
import dev.evans.daos.ReimbursementDAOImpl;
import dev.evans.models.Reimbursement;
import dev.evans.models.Status;
import dev.evans.util.IOUtil;

public class ReimbursementServiceImpl implements ReimbursementService {

	// reference to the ReimbursementDAO singleton
	private ReimbursementDAO rDAO;
	
	// singleton reference to ReimbursementService
	private static final ReimbursementService rService = new ReimbursementServiceImpl();
	
	public ReimbursementServiceImpl() {
		super();
		rDAO = ReimbursementDAOImpl.getReimbursementDAO();
	}
	
	// returns singleton reference
	public static ReimbursementService getReimbursementService() {
		return rService;
	}

	@Override
	public boolean submitNewReimbursement(Reimbursement rb) {
		boolean success = false;
		// first check that the reimbursement object isn't null
		if(rb != null) {
			// then check that the reimbursement object has valid values
			if(rb.getEmployeeId() > 0 && rb.getPurchaseDate() != null && rb.getDescription() != null && rb.getTotalCost() > 0 && rb.getTotalCost() <= IOUtil.MAX_COST) {
				// finally check that date string and total cost are formatted correctly
				if(rb.getPurchaseDate().matches("^\\d{4}-\\d{2}-\\d{2}$") && Double.toString(rb.getTotalCost()).matches("^\\d+\\.\\d{0,2}$")) {
					success = rDAO.addReimbursement(rb);
				}
			}
		}
		return success;
	}

	@Override
	public List<Reimbursement> findPendingReimbursementsByUserId(int userId) {
		List<Reimbursement> rbs = null;
		// first check that the userId is positive
		if(userId > 0) {
			rbs = rDAO.getPendingReimbursementsByUserId(userId); // get the employee's pending reimbursements
		}
		return rbs;
	}

	@Override
	public List<Reimbursement> findResolvedReimbursementsByUserId(int userId) {
		List<Reimbursement> rbs = null;
		// first check that the userId is positive
		if(userId > 0) {
			rbs = rDAO.getResolvedReimbursementsByUserId(userId); // get the employee's resolved reimbursements
		}
		return rbs;
	}

	@Override
	public List<Reimbursement> findAllPendingReimbursements() {
		return rDAO.getAllPendingReimbursements();
	}

	@Override
	public List<Reimbursement> findAllResolvedReimbursements() {
		return rDAO.getAllResolvedReimbursements();
	}

	@Override
	public boolean resolveReimbursement(int reimbId, int managerId, Status status) {
		boolean success = false;
		// first check that the parameters are valid
		if(reimbId > 0 && managerId > 0 && status != null) {
			success = rDAO.updateReimbursementById(reimbId, managerId, status);
		}
		return success;
	}

}
