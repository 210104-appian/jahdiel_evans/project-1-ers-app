package dev.evans.services;

import java.util.List;

import dev.evans.daos.UserDAO;
import dev.evans.daos.UserDAOImpl;
import dev.evans.models.User;
import dev.evans.models.UserRole;

public class UserServiceImpl implements UserService {
	
	// reference to the UserDAO singleton
	private UserDAO uDAO;
	
	// singleton reference to UserService
	private static final UserService uService = new UserServiceImpl();

	private UserServiceImpl() {
		super();
		uDAO = UserDAOImpl.getUserDAO();
	}
	
	// returns singleton reference
	public static UserService getUserService() {
		return uService;
	}

	@Override
	public User verifyUser(String username, String password, UserRole loginRole) {
		User matchingUser = null;
		// check that none of the parameters are null before verifying the user
		if(username != null && password != null && loginRole != null) {
			// search for a user in the database that matches the passed in credentials
			matchingUser = uDAO.getUserByCredentials(username, password);
			// if a user was found that matches the credentials, check if the user has the right role/permission to log in
			if(matchingUser != null) {
				List<UserRole> possibleRoles = uDAO.getUserRoles(matchingUser.getUserId());
				// if the user has the right role to log in, then set that as the user's current role
				if(possibleRoles.contains(loginRole)) {
					matchingUser.setCurrentRole(loginRole);
				}
				else { // else the user doesn't have the required role
					matchingUser.setCurrentRole(null);
				}
			}
		}
		return matchingUser;
	}
	
	@Override
	public User findUserById(int userId) {
		User matchingUser = null;
		// check that the userId is a valid positive integer
		if(userId > 0) {
			// search for a user in the database that matches the passed in credentials
			matchingUser = uDAO.getUserById(userId);
			// if a user was found that matches the ID, get their highest role
			if(matchingUser != null) {
				matchingUser.setCurrentRole(uDAO.getHighestRole(userId));
			}
		}
		return matchingUser;
	}

}
