package dev.evans.services;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import dev.evans.models.User;
import dev.evans.models.UserRole;

public class UserServiceImplTest {

	UserService userService = UserServiceImpl.getUserService();
	
	// 6 edge case tests and 4 valid parameter tests for verifyUser() method
	@Test
	public void testVerifyUserWithNullValues() {
		User expected = null;
		User actual = userService.verifyUser(null, null, null);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserWithEmptyStrings() {
		User expected = null;
		User actual = userService.verifyUser("", "", UserRole.EMPLOYEE);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserWithAllWrongCreds() {
		User expected = null;
		User actual = userService.verifyUser("Bobby23", "P4ssW0rd", UserRole.EMPLOYEE);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserWithWrongUsername() {
		User expected = null;
		User actual = userService.verifyUser("Samuel45", "28Yassy$37", UserRole.EMPLOYEE);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserWithWrongPassword() {
		User expected = null;
		User actual = userService.verifyUser("BensonW12", "P4ssW0rd", UserRole.EMPLOYEE);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserWithEmployeeLoginAsManager() {
		User expected = new User();
		// user William Oneil has a user ID of 5 and an employee user role
		expected.setUserId(5);
		expected.setFirstName("William");
		expected.setLastName("Oneil");
		// A null current role represents insufficient user privileges for the login
		expected.setCurrentRole(null);
		User actual = userService.verifyUser("OneilW67", "Willy%4893", UserRole.MANAGER);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserWithManagerLoginAsEmployee() {
		User expected = new User();
		// user Samantha Kaneko has a user ID of 2 and both employee and manager user roles
		expected.setUserId(2);
		expected.setFirstName("Samantha");
		expected.setLastName("Kaneko");
		// A current role equal to the login role represents sufficient user privileges for the login
		expected.setCurrentRole(UserRole.EMPLOYEE);
		User actual = userService.verifyUser("KanekoS95", "55&ILuvDog$", UserRole.EMPLOYEE);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserWithCaseInsensitiveUsername() {
		User expected = new User();
		// user Jennifer Perez has a user ID of 1 and an employee user role
		expected.setUserId(1);
		expected.setFirstName("Jennifer");
		expected.setLastName("Perez");
		// A current role equal to the login role represents sufficient user privileges for the login
		expected.setCurrentRole(UserRole.EMPLOYEE);
		User actual = userService.verifyUser("PEREZJ84", "$981OwlGal", UserRole.EMPLOYEE);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserWithEmployeeLoginAsEmployee() {
		User expected = new User();
		// user William Oneil has a user ID of 5 and an employee user role
		expected.setUserId(5);
		expected.setFirstName("William");
		expected.setLastName("Oneil");
		// A current role equal to the login role represents sufficient user privileges for the login
		expected.setCurrentRole(UserRole.EMPLOYEE);
		User actual = userService.verifyUser("OneilW67", "Willy%4893", UserRole.EMPLOYEE);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVerifyUserWithManagerLoginAsManager() {
		User expected = new User();
		// user Samantha Kaneko has a user ID of 2 and both employee and manager user roles
		expected.setUserId(2);
		expected.setFirstName("Samantha");
		expected.setLastName("Kaneko");
		// A current role equal to the login role represents sufficient user privileges for the login
		expected.setCurrentRole(UserRole.MANAGER);
		User actual = userService.verifyUser("KanekoS95", "55&ILuvDog$", UserRole.MANAGER);
		assertEquals(expected, actual);
	}
	
}
