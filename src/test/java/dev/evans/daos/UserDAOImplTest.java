package dev.evans.daos;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import dev.evans.models.User;
import dev.evans.models.UserRole;

public class UserDAOImplTest {

	UserDAO userDAO = UserDAOImpl.getUserDAO();
	
	
	// 5 edge case tests and 2 valid parameter tests for getUserByCredentials() method
	@Test
	public void testGetUserByCredWithNullValues() {
		User expected = null;
		User actual = userDAO.getUserByCredentials(null, null);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetUserByCredWithEmptyStrings() {
		User expected = null;
		User actual = userDAO.getUserByCredentials("", "");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetUserByCredWithAllWrongCreds() {
		User expected = null;
		User actual = userDAO.getUserByCredentials("Bobby23", "P4ssW0rd");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetUserByCredWithWrongUsername() {
		User expected = null;
		User actual = userDAO.getUserByCredentials("Samuel45", "28Yassy$37");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetUserByCredWithWrongPassword() {
		User expected = null;
		User actual = userDAO.getUserByCredentials("BensonW12", "P4ssW0rd");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetUserByCredWithCaseInsensitveUsername() {
		User expected = new User();
		expected.setUserId(5); // user William Oneil has a user ID of 5
		expected.setFirstName("William");
		expected.setLastName("Oneil");
		User actual = userDAO.getUserByCredentials("oneilw67", "Willy%4893");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetUserByCredWithValidCreds() {
		User expected = new User();
		expected.setUserId(4); // user Yasmine Parker has a user ID of 4
		expected.setFirstName("Yasmine");
		expected.setLastName("Parker");
		User actual = userDAO.getUserByCredentials("ParkerY43", "28Yassy$37");
		assertEquals(expected, actual);
	}
	
	// 3 edge case tests and 1 valid parameter test for getUserRoles() method
	@Test
	public void testGetUserRolesWithNegativeId() {
		List<UserRole> expected = new ArrayList<>();
		List<UserRole> actual = userDAO.getUserRoles(-5);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetUserRolesWithZeroId() {
		List<UserRole> expected = new ArrayList<>();
		List<UserRole> actual = userDAO.getUserRoles(0);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetUserRolesWithInvalidPositiveId() {
		List<UserRole> expected = new ArrayList<>();
		List<UserRole> actual = userDAO.getUserRoles(9999);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetUserRolesWithValidId() {
		List<UserRole> expected = new ArrayList<>();
		expected.add(UserRole.EMPLOYEE); // user Jennifer Perez (user ID: 1) has only EMPLOYEE role
		List<UserRole> actual = userDAO.getUserRoles(1);
		assertEquals(expected, actual);
	}
	
}
