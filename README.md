# Reployee Expense Reimbursement System (ERS) - Java

## Project Description

The Reployee Expense Reimbursement System (ERS) manages the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

## Technologies Used

* Java 1.8
* Servlets
* JDBC
* SQL
* HTML
* CSS
* Javascript
* Bootstrap
* AJAX
* JUnit
* Log4j

## Environment

* Apache Tomcat v9.0 Server
* Oracle Database
* Eclipse/Spring Tools Suite 4
* DBeaver

## Features
Implemented User Stories:
* An Employee can login
* An Employee can view the Employee Homepage
* An Employee can logout
* An Employee can submit a reimbursement request
* An Employee can view their pending reimbursement requests
* An Employee can view their resolved reimbursement requests
* A Manager can login
* A Manager can view the Manager Homepage
* A Manager can logout
* A Manager can approve/deny pending reimbursement requests
* A Manager can view all pending requests from all employees
* A Manager can view all resolved requests from all employees and see which manager resolved it
* Unit testing of business logic/data validation using JUnit
* Logging of significant runtime events using Log4j

To-do List:
* An Employee can view their information (profile)
* An Employee can update their information
* An Employee can upload an image of his/her receipt as part of the reimbursement request
* A Manager can view all Employees

## Getting Started
The following instructions are for setting up and running the Java web application on a Tomcat v9.0 server in Eclipse/Spring Tool Suite 4:
1. Clone the project's Git repository in a directory of your choosing using the following Git command: `git clone https://gitlab.com/210104-appian/jahdiel_evans/project-1-ers-app.git`.
2. Download and extract the binary distribution zip file for Apache Tomcat v9.0 in a directory of your choosing from the following webpage: [Apache Tomcat 9](https://tomcat.apache.org/download-90.cgi).
3. Within the IDE, Right-click in the Package Explorer tab, select **Import > Maven > Existing Maven Projects**, click on **Browse**, select the project folder you cloned named ers-app, and click **Finish** to import the Maven project.
4. Right-click the newly imported project folder named ers-app and select **Maven > Update Project** to update the Maven project and download any required dependencies from the pom.xml file.
5. In the menu bar, go to **Window > Show View > Other**, select **Servers**, and click **Open** to display the server view tab.
6. Right-click on the server view and a pop-up menu will be appear. From there, choose **New > Server**.
7. Choose **Apache > Tomcat v9.0** and click on **Next**.
8. Click on **Browse**, select the path location where the extracted version of Tomcat v9.0 exists, and click on **Finish** to create the server.
9. To add the Java web application to the server, right-click the Tomcat v9.0 Server at localhost, select **Add and Remove**, add the ers-app as a web module, and click **Finish**.
10. Finally, run the server by right-clicking it and selecting **Start**.
11. To theoretically set up the connection to the Oracle Database, go to **Run > Run Configurations** in the menu bar and set up the Environment Variables for the Tomcat v9.0 Server at localhost to include variables for the DB_PASSWORD, DB_URL, and DB_USERNAME. This allows for a secure connection to the database without needing to hardcode the database's URL and login credentials. **NOTE:** the Oracle Database used initially for this project no longer exists and will instead be replaced with static data within the Java application.

## Usage

To access the login page for the web application, simply navigate to the following URL using a web browser while the Tomcat v9.0 web server is running on your computer as localhost: [Reployee ERS Login Page](http://localhost:8080/ers-app/login).

From there, you should be able to login as an employee or manager using either of the following demo credentials:
* Employee Demo Credentials
    * Username: PerezJ84
    * Password: $981OwlGal
* Manager Demo Credentials
    * Username: BensonW12
    * Password: BenRules48*

Once logged in, an employee has the following actions available at their dashboard:
* Create New Request
* View Pending Requests
* View Resolved Requests

Managers on the other hand have these actions available at their admin dashboard:
* Resolve Pending Requests
* View All Pending Requests
* View All Resolved Requests

## Contributors

N/A (solo project)

## License

This project uses the following license: [MIT License](https://gitlab.com/210104-appian/jahdiel_evans/project-1-ers-app/-/blob/master/LICENSE).
